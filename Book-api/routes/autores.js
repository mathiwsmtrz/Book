var express = require('express');
var router = express.Router();
var autorController = require('../controllers/autor.controller')


router.get('/', function(req, res, next) {
    autorController.listar(req, res);
});
router.post('/', function(req, res, next) {
    autorController.guardar(req, res);
});
router.put('/', function(req, res, next) {
    autorController.actualizar(req, res);
});
router.delete('/:codigo', function(req, res, next) {
    autorController.eliminar(req, res);
});

module.exports = router;
