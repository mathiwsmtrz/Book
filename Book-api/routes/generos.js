var express = require('express');
var router = express.Router();
var generoController = require('../controllers/genero.controller')


router.get('/:codigo', function(req, res, next) {
    generoController.listar(req, res);
});
router.post('/', function(req, res, next) {
    generoController.guardar(req, res);
});
router.put('/:codigo', function(req, res, next) {
    generoController.actualizar(req, res);
});
router.delete('/:codigo', function(req, res, next) {
    generoController.eliminar(req, res);
});

module.exports = router;
