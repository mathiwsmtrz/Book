var express = require('express');
var router = express.Router();
var libroController = require('../controllers/libro.controller')


router.get('/', function(req, res, next) {
    libroController.listar(req, res);
});
router.post('/', function(req, res, next) {
    libroController.guardar(req, res);
});
router.put('/:codigo', function(req, res, next) {
    libroController.actualizar(req, res);
});
router.delete('/:codigo', function(req, res, next) {
    libroController.eliminar(req, res);
});

module.exports = router;
