var mongoose = require('mongoose');
var Schema = mongoose.Schema;

 
var generoModel = new Schema({
    "codigo"    : { type: String, require: true, unique: true },
    "nombre"    : { type: String, require: true, unique: true },
    "edad"      : { type: String, require: false, unique:false}
});

module.exports = mongoose.model('genero', generoModel);