var mongoose = require('mongoose');
var Schema = mongoose.Schema;

 
var autorModel = new Schema({
    codigo              : { type: String, require: true, unique: true },
    nombre              : { type: String, require: true, unique: true },
    fecha_nacimiento    : { type: Date, require:true},
    ciudad              : { type: String, require:true},
    telefono              : { type: String, require:true}
});

module.exports = mongoose.model('autor', autorModel);