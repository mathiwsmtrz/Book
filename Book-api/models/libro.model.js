var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var autorModel = require('../models/autor.model');
var generoModel = require('../models/genero.model');

 
var libroModel = new Schema({
    codigo              : { type: String, require: true, unique: true },
    nombre              : { type: String, require: true, unique: true },
    autor               : { type: Schema.ObjectId, ref: "autorModel" },
    genero              : { type: Schema.ObjectId, ref: "generoModel" },
    fecha_publicacion   : { type: Date, require: true},
    url_imagen          : { type: String, require: false},
    descripcion         : { type: String, require:false},
    estado              : { type: Number, default: 1 },
    created_at          : { type: Date, default: Date.now },
    update_at           : { type: Date }
});

module.exports = mongoose.model('libroModel', libroModel);