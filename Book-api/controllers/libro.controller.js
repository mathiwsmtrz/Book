var libroModel = require('../models/libro.model.js');
var autorModel = require('../models/autor.model');
module.exports = {
    listar: function (req, res) {
        libroModel.find({}, function (err, libros) {
            autorModel.populate(libros, {path: "autor"},function(err, libros){
                
                if (err) {
                    res.status(500).json({
                        message: 'erro al listar libros',
                        err: err
                    })
                } else {
                    res.json(libros)
                }
            });
        })
    },
    guardar: function (req, res) {
        var libro = new libroModel({
            codigo: req.body.codigo,
            nombre: req.body.nombre,
            autor: req.body.autor,
            genero: req.body.genero,
            fecha_publicacion: req.body.fecha_publicacion,
            url_imagen: req.body.url_imagen,
            descripcion: req.body.descripcion
        })

        libro.save(function (err, libro) {
            if (err) {
                res.status(500).json({
                    message: 'erro al guardar libros',
                    err: err
                })
            } else {
                res.json({
                    message: 'Libro guardado correctamente',
                    _id: libro._id
                })
            }
        })
    },
    actualizar: function (req, res) {
        libroModel.findOneAndUpdate({
            _id: req.params.codigo
        }, {

                $set: {
                    codigo: req.body.codigo,
                    nombre: req.body.nombre,
                    autor: req.body.autor,
                    genero: req.body.genero,
                    fecha_publicacion: req.body.fecha_publicacion,
                    url_imagen: req.body.url_imagen,
                    descripcion: req.body.descripcion
                }
            }, function (err, libro) {
                if (err) {
                    res.status(500).json({
                        message: 'error al actualizar libros',
                        err: err
                    })
                } else {
                    res.json({
                        message: 'Libro actualizado correctamente',
                        _id: libro._id
                    })
                }
            })
    },
    eliminar: function (req, res) {
        libroModel.remove({
            _id: req.params.codigo
        }, function(err, libro) {
            if (err) {
                res.status(500).json({
                    message: "Error al eliminar este libro",
                    error: err
                });
            }

            res.json({
                message: "Este libro se elimino correctamente",
                data: libro
            });
        });
    }
}