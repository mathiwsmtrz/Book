var autorModel = require('../models/autor.model');

module.exports = {
    listar: function (req, res) {
        autorModel.find({}, function (err, autores) {
            if (err) {
                res.status(500).json({
                    message: "Error al listar autores",
                    error: err
                })
            } else {
                res.json(autores)
            }
        })
    },
    guardar: function (req, res) {
        console.log(req.body)
        autorModel.findOne({
            nombre: req.body.nombre
        }, function (err, resultados) {
            console.log(resultados)
            if (resultados == '') {
                res.status(400).json({
                    message: 'Este autor ya existe'
                });
            } else {
                var autor = new autorModel({
                    codigo: req.body.codigo,
                    nombre: req.body.nombre,
                    fecha_nacimiento: req.body.fecha_nacimiento,
                    ciudad: req.body.ciudad,
                    telefono: req.body.telefono
                });

                autor.save(function (err, autor) {
                    if (err) {
                        res.status(500).json({
                            message: 'error al guardar el autor',
                            error: err
                        });
                    }

                    res.json({
                        message: 'Autor guardado correctamente',
                        action: true,
                        _id: autor._id
                    });
                })
            }
        })
    },
    actualizar: function (req, res) {
        autorModel.findOne({
            codigo: req.body.codigo
        }, function (err, resultados) {
            if (resultados) {
                autorModel.findOneAndUpdate(
                    { _id: req.body._id }, {
                        $set: {
                            codigo: req.body.codigo,
                            nombre: req.body.nombre,
                            fecha_nacimiento: req.body.fecha_nacimiento,
                            ciudad: req.body.ciudad,
                            telefono: req.body.telefono
                        }
                    }, function (err, autor) {
                        if (err) {
                            res.status(500).json({
                                message: 'error al actualizar el autor',
                                error: err
                            });
                        }

                        res.json({
                            message: 'Autor actualizado correctamente',
                            action: true,
                            _id: autor._id
                        });
                    })
            } else {
                res.status(404).json({
                    message: 'Este autor no existe'
                });
            }
        })
    },
    eliminar: function (req, res) {
        autorModel.remove({
            _id: req.params.codigo
        }, function (err, autor) {
            if (err) {
                res.status(500).json({
                    message: "Error al eliminar este autor",
                    error: err
                });
            }

            res.json({
                message: "Este autor se elimino correctamente",
                data: autor
            });
        });
    }
}