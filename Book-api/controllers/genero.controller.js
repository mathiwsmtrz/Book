var generoModel = require('../models/genero.model.js');

module.exports = {
    listar: function(req, res) {

        if (req.params.codigo == 0) {
            generoModel.find({}, function(err, generos) {
                if (err) {
                    res.status(500).json({
                        message: "Error listadno generos",
                        error: err
                    });
                }

                res.json(generos);

            });
        } else {

            generoModel.findOne({
                codigo: req.params.codigo
            }, function(err, genero) {
                if (err) {
                    res.status(500).json({
                        message: "Error al buscar este genero",
                        error: err
                    });
                }

                res.json(genero);

            });

        }



    },
    guardar: function(req, res) {

        generoModel.find({
            nombre: req.body.nombre,
            codigo: req.body.codigo
        }, function(err, genero) {
            if (err) {
                res.status(500).json({
                    message: 'error al cargar genero',
                    error: err
                });
            }

            if (genero) {
              res.status(400).json({
                  message: 'este genero ya existe'
              });
            }else{
              var genero = new generoModel({
                  codigo: req.body.codigo,
                  nombre: req.body.nombre

              });

              genero.save(function(err, genero) {
                  if (err) {
                      res.status(500).json({
                          message: 'error al guardar el genero',
                          error: err
                      });
                  }

                  res.json({
                      message: 'Genero guardado correctamente',
                      action: true,
                      code: genero.codigo,
                      _id: genero._id
                  });

              });
            }
        })

    },
    actualizar: function(req, res) {

        generoModel.findOneAndUpdate({
            _id: req.body._id
        }, {
            $set: {
                codigo: req.body.codigo,
                nombre: req.body.nombre
            }
        }, function(err, generos) {
            if (err) {
                res.status(500).json({
                    message: "Error al actualizar este genero",
                    error: err
                });
            } else {
                res.json({
                    message: "Genero actualizado correctamente",
                    action: true
                });
            }

        });

    },
    eliminar: function(req, res) {

        generoModel.remove({
            codigo: req.params.codigo
        }, function(err, genero) {
            if (err) {
                res.status(500).json({
                    message: "Error al eliminar este genero",
                    error: err
                });
            }

            res.json({
                message: "Este genero se elimino correctamente",
                data: genero
            });
        });

    }
}
