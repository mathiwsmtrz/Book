export class Libro {
    constructor(
        public _id: string,
        public codigo: number,
        public nombre: string,
        public autor: string,
        public genero: string,
        public fecha_publicacion: string,
        public url_imagen: string,
        public descripcion: string
    ) { }
}