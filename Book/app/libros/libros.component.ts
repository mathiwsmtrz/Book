import { Component, OnInit } from '@angular/core';
import { NgForm }    from '@angular/forms';

import { Http }    from '@angular/http';

import { Libro } from './libros.class';

@Component({
    selector: 'libros',
    templateUrl: './app/libros/libros.component.html'
})
export class librosComponent implements OnInit{

    public generosList = [''];
    public autoresList = [''];
    public librosList = [''];
    public editForm: boolean = false;

    libroModel = new Libro('', new Date().getTime(),'','','','','','');

    ngOnInit(){
        this.cargar_autores();
        this.cargar_generos();
        this.cargar_libros();
    }

    constructor(private http: Http) { }

    cargar_autores(){
        this.http.get('http://localhost:3000/autores').subscribe(
            res => {
                this.autoresList = res.json();
            }
        )
    }

    cargar_generos(){
        this.http.get('http://localhost:3000/generos/0').subscribe(
            res => {
                this.generosList = res.json();
            }
        )
    }

    cargar_libros(){
        this.http.get('http://localhost:3000/libros').subscribe(
            res => {
                this.librosList = res.json();
            }
        )
    }

    editar_libro(item:any){
        this.editForm = true;
        this.libroModel = new Libro(item._id, item.codigo, item.nombre, item.autor,item.genero, item.fecha_publicacion,item.url_imagen, item.descripcion)
    }

    OnSubmit(){
       if (this.editForm) {
           this.http.put('http://localhost:3000/libros/'+this.libroModel._id, this.libroModel).subscribe(
            res => {
                alert(res.json().message);
                this.libroModel = new Libro('', new Date().getTime(),'','','','','','');
                this.cargar_libros();
            },
            err => {
                console.log(err)
            }
        )
       } else{
           this.http.post('http://localhost:3000/libros', this.libroModel).subscribe(
            res => {
                alert(res.json().message);
                this.libroModel = new Libro('', new Date().getTime(),'','','','','','');
                this.cargar_libros();
            },
            err => {
                console.log(err)
            }
        )
       }
    }

    eliminar_libro(item: any) {
        this.http.delete('http://localhost:3000/libros/'+item).subscribe(
            res => {
                alert(res.json().message)
                this.cargar_libros();
            },
            err => {
                console.log(err)
            }
        )
    }

    cancelar(){
       this.libroModel = new Libro('', new Date().getTime(),'','','','','','');
       this.editForm= false;
    }


}