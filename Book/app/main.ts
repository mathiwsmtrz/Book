import { bootstrap }    from '@angular/platform-browser-dynamic';
import { appRouterProviders } from './app.routes';
import { disableDeprecatedForms, provideForms } from '@angular/forms';

import { AppComponent } from './app/app.component';

import { HTTP_PROVIDERS } from '@angular/http'

bootstrap(AppComponent,[
    appRouterProviders,
    disableDeprecatedForms(),
    provideForms(),
    HTTP_PROVIDERS
]);
