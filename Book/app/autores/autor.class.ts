export class Autor {
    constructor(
        public _id: string,
        public codigo: number,
        public nombre: string,
        public fecha_nacimiento : string,      
        public ciudad: string,
        public telefono: string
    ) {
        
    }
}