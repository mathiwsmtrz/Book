import { Component, OnInit } from '@angular/core';
import { NgForm }    from '@angular/forms';

import { Http }    from '@angular/http';

import { Autor } from './autor.class';

@Component({
    selector: 'autores',
    templateUrl: 'app/autores/autores.component.html'
})
export class AutoresComponent implements OnInit {

    autoresModel = new Autor('', new Date().getTime(), '', '', '', '')
    public autoresList = [''];
    public editForm: boolean = false;

    ngOnInit() {
        this.cargar_autores()
    }

    constructor(private http: Http) { }

    cargar_autores() {
        this.http.get('http://localhost:3000/autores/').subscribe(
            res => {
                this.autoresList = res.json();
            }
        )
    }

    OnSubmit() {
        if (this.editForm) {

            this.http.put('http://localhost:3000/autores', this.autoresModel).subscribe(
                res => {
                    alert(res.json().message)
                    this.cargar_autores();
                    this.autoresModel = new Autor('', new Date().getTime(), '', '', '', '')
                },
                err => {
                    console.log(err)
                }
            )
        } else {
            this.http.post('http://localhost:3000/autores', this.autoresModel).subscribe(
                res => {
                    alert(res.json().message)
                    this.cargar_autores();
                    this.autoresModel = new Autor('', new Date().getTime(), '', '', '', '')
                },
                err => {
                    console.log(err)
                }
            )

        }
    }

    editar_autor(item: any) {
        let dat : any = new Date(item.fecha_nacimiento);
        this.autoresModel = new Autor(item._id, item.codigo, item.nombre, dat, item.ciudad, item.telefono)
        this.editForm = true;
    }

    eliminar_autor(item: any) {
        this.http.delete('http://localhost:3000/autores/'+item).subscribe(
            res => {
                alert(res.json().message)
                this.cargar_autores();
            },
            err => {
                console.log(err)
            }
        )
    }

    cancelar(){
        this.autoresModel = new Autor('', new Date().getTime(), '', '', '', '')
        this.editForm= false;
    }



}