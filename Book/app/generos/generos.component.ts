import { Component, OnInit } from '@angular/core';
import { NgForm }    from '@angular/forms';

import { Http }    from '@angular/http';

@Component({
    selector: 'generos',
    templateUrl: './app/generos/generos.component.html'
})
export class generosComponent implements OnInit {

    public generoModel: any = { _id: '', codigo: '', nombre: '' };
    public generosList: any = [''];
    public editForm: boolean = false;

    public class:any = { _id: '', codigo: '', nombre: '' };

    constructor(private http: Http) {

    }

    ngOnInit() {
        this.cargarCategorias()
    }

    cargarCategorias() {

        this.http.get('http://localhost:3000/generos/0').subscribe(
            res => {
                this.generosList = res.json();
            }
        )

    }

    agregarCategoria() {

        if (this.editForm) {
            this.http.put('http://localhost:3000/generos/' + this.generoModel._id, this.generoModel).subscribe(
                res => {
                    let data = res.json()
                    alert(data.message)
                    this.cargarCategorias();
                    this.cancelar();
                },
                err => {
                    let error = err.json()
                    alert(error.message)
                    console.log(error)
                }
            )
        } else {
            this.http.post('http://localhost:3000/generos', this.generoModel).subscribe(
                res => {
                    let data = res.json()
                    alert(data.message)
                    this.cargarCategorias();
                    this.cancelar()
                },
                err => {
                    let error = err.json()
                    alert(error.message)
                    console.log(error)
                }
            )           

        }





    }

    editarGenero(item: any) {
        this.generoModel = item;
        this.editForm = true;
    }

    eliminarGenero(item: number) {
        this.http.delete('http://localhost:3000/generos/' + item).subscribe(
            res => {
                var data = res.json()
                alert(data.message)
                this.cargarCategorias();
            },
            err => {
                console.log(err.json())
            }
        )
    }

    cancelar() {
        this.generoModel = { _id: '', codigo: '', nombre: '' };
        this.editForm = false;
    }


}