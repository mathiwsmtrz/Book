import { provideRouter, RouterConfig }  from '@angular/router';

import { generosComponent } from './generos/generos.component';
import { librosComponent } from './libros/libros.component';
import { indexComponent } from './index/index.component';
import { AutoresComponent } from './autores/autores.component';

const routes: RouterConfig = [
  {
    path: '',
    redirectTo: '/inicio',
    pathMatch: 'full'
  },
  {
    path: 'inicio',
    component: indexComponent
  },
  {
    path: 'libros',
    component: librosComponent
  },
  {
    path: 'generos',
    component: generosComponent
  },
  {
    path: 'autores',
    component: AutoresComponent
  }
];

export const appRouterProviders = [
  provideRouter(routes)
];