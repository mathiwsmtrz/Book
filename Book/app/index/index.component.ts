import { Component, OnInit } from '@angular/core';

import { Http }    from '@angular/http';

@Component({
    selector: 'libros',
    templateUrl: './app/index/index.component.html'
})
export class indexComponent implements OnInit{
    
    public librosList= ['']
    
    ngOnInit(){
        this.cargar_libros();
    }
    
    constructor(private http: Http) { }
    
    cargar_libros(){
        this.http.get('http://localhost:3000/libros').subscribe(
            res => {
                this.librosList = res.json();
            }
        )
    }

}